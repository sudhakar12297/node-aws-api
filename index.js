const express = require("express");
const jwt = require("jsonwebtoken");
var bodyParser = require("body-parser");

const { credentials } = require("./utils/userDetails");

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const port = 3000;
const secretKey = "itsaeydemoapp";

app.get("/", (req, res) => {
  res.send("Hello World, from express");
});

app.post("/login", async (req, res, next) => {
  let { username, password } = req.body;
  let isValidCredentials;
  try {
    isValidCredentials =
      username === credentials.username && password === credentials.password;
  } catch {
    const error = new Error("Error! Something went wrong.");
    return next(error);
  }
  if (!isValidCredentials) {
    res.status(401).json({
      success: false,
      data: {
        msg: "Invalid credentials!!!",
      },
    });
    return;
  }
  let token;
  try {
    //Creating jwt token
    token = jwt.sign({ username }, "itsaeydemoapp", { expiresIn: 60 });
  } catch (err) {
    console.log(err);
    const error = new Error("Error! Something went wrong.");
    return next(error);
  }

  res.status(200).json({
    success: true,
    data: {
      username,
      token,
    },
  });
});

app.get("/companies", (req, res) => {
  const token = req.headers.authorization.split(" ")[1];
  if (!token) {
    res.status(401).json({
      success: false,
      message: "Unauthorized, Please provide valid token.",
    });
    return;
  }
  try {
    const decodedToken = jwt.verify(token, "itsaeydemoapp");
    res.status(200).json({
      success: true,
      data: {
        companies: [
          {
            name: "EY",
            location: "Chennai",
            domain: "Finance",
            tech: "Mendix",
          },
          {
            name: "NT",
            location: "Bangalore",
            domain: "Banking",
            tech: "Mendix",
          },
          {
            name: "Optum",
            location: "Chennai",
            domain: "Healthcare",
            tech: "React",
          },
        ],
      },
    });
    return;
  } catch (err) {
    res.status(401).json({
      success: false,
      data: {
        msg: "Invalid token!!!",
      },
    });
  }
});

app.listen(port, () =>
  console.log(`App listening on port ${port}!`)
);
